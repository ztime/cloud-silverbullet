# Silverbullet setup

The idea is to have a setup with Silverbullet hosted in the cloud, under some domain and behind some auth

Components
* Silverbullet
* Traefik

## Steps so far:
* Copy the configs and users_database files to create your own `inventory.example.ini -> inventory.ini, config.example.yml -> config.yml, authelia-config/users_database.example.yml -> users_database.yml`
* Create an instance on EC2 (smallest free tier runs absolutely fine)
* Download the pem and write the user id (probably `ec2-user`) into `inventory.ini`
* Go to https://www.duckdns.org/ and create your subdomain
* Configure your subdomain with the ip of your instance
* Copy your duckdns token and insert into `config.yml`
* Populate your `users_database.yml` with your users
* Run the playbook with `ansible-playbook -i inventory.ini playbook.yaml`

